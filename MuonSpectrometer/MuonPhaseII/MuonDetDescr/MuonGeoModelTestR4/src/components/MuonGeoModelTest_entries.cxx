
/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../GeoModelMdtTest.h"
#include "../GeoModelRpcTest.h"


DECLARE_COMPONENT(MuonGMR4::GeoModelMdtTest)
DECLARE_COMPONENT(MuonGMR4::GeoModelRpcTest)